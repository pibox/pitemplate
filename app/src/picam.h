/*******************************************************************************
 * picam
 *
 * picam.h:  program main
 *
 * License: GPL v3
 *
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#ifndef PICAM_H
#define PICAM_H

#include <gtk/gtk.h>

#define KEYSYMS_F     "/etc/pibox-keysyms"
#define KEYSYMS_FD    "data/pibox-keysyms"

/* Message Types for piboxd */
#define MT_STREAM    1

/* Message actions (byte 2 of PIBOX_MSG_T header): MT_STREAM */
#define MA_START     1
#define MA_END       2

/*========================================================================
 * Globals
 *=======================================================================*/
#ifdef PICAM_C
int     daemonEnabled = 0;
char    errBuf[256];
#else
extern int      daemonEnabled;
extern char     errBuf[];
#endif /* PICAM_C */

/*========================================================================
 * Defined values
 *=======================================================================*/
#define PROG        "picam"
#define MAXBUF      4096

// Where the config file is located
#define F_CFG   "/etc/picam.cfg"
#define F_CFG_T "data/picam.cfg"

/*========================================================================
 * Prototypes
 *=======================================================================*/
#ifndef PICAM_C
#endif

/*========================================================================
 * Include other headers
 *=======================================================================*/
#include <pibox/log.h>
#include "player.h"
#include "cli.h"
#include "utils.h"

#endif /* !PICAM_H */

