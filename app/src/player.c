/*******************************************************************************
 * picam
 *
 * player.c:  Start and stop webcam.
 *
 * License: see LICENSE file
 ******************************************************************************
 * Notes:
 * Being a C file (and not Java), please adhere to the 80 character line
 * length standard in this file.
 ******************************************************************************/
#define PLAYER_C

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

static int playerIsRunning = 0;
static pthread_mutex_t playerProcessorMutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_t playerProcessorThread;
static pid_t videoPid = -1;


#include "picam.h"

/*========================================================================
 * Name:   spawnPlayer
 * Prototype:  int spawnPlayer( char *filename )
 *
 * Description:
 * Start the webcam on the localhost mjpeg-streamer stream.
 *
 * Returns:
 * The process id of the video player or -1 if the player cannot be started.
 *========================================================================*/
pid_t
spawnPlayer( char *filename )
{
    pid_t       childPid;
    char        *cmd, *str;
    char        fullpath[MAXBUF];
    char        *args[64];
    char        *arg;
    char        *ptr;
    int         filepos;
    int         found;
    int         i;

    // Safety check
    if ( filename == NULL )
    {
        piboxLogger(LOG_ERROR, "Missing filename, can't spawn video.\n");
        return -1;
    }
    piboxLogger(LOG_INFO, "Filename: %s\n");

    // Find the appropiate player command
    piboxLogger(LOG_INFO, "Looking for player for format: any\n");
    cmd = findPlayer("any");
    if ( cmd == NULL )
    {
        piboxLogger(LOG_INFO, "Can't find player for format: any\n");
        return -1;
    }
    piboxLogger(LOG_INFO, "Found player for format: %s\n", cmd);

    // Fill in path buffer
    sprintf(fullpath, "%s", filename);

    // Create command line that will run the process.
    piboxLogger(LOG_INFO, "Starting video: %s, path = %s\n", cmd, fullpath);
    childPid = fork();
    if ( childPid == 0 )
    {   
        // Child: start player for video
        str = g_strdup(cmd);
        filepos = parse(str, args, 64);
        piboxLogger(LOG_INFO, "filepos = %d\n", filepos);

        /*
         * Replace %s with path to video.
         */
        found = 0;
        for(i=0; i<filepos; i++)
        {
            piboxLogger(LOG_TRACE1, "args[%d] = %s\n", i, args[i]);
            if ( strstr(args[i], "%s") != NULL )
            {
                arg = g_malloc0(strlen(args[i])+strlen(fullpath));
                sprintf(arg, "%s", args[i]);
                ptr = arg;
                while (*ptr != '%' )
                    ptr++;
                memcpy(ptr, fullpath, strlen(fullpath));
                args[i] = arg;
                found = 1;
                break;
            }
        }
        if ( found )
        {
            execvp(args[0], args);
            piboxLogger(LOG_ERROR, "Failed to spawn video player cmd %s, path = %s: %s\n", 
                    cmd, fullpath, strerror(errno));
        }
        else
        {
            piboxLogger(LOG_ERROR, "Missing \\%s argument in player cmd: %s\n", cmd);
        }
        abort();
    }

    return childPid;
}

/*========================================================================
 * Name:   killPlayer
 * Prototype:  int killPlayer( pid_t pid)
 *
 * Description:
 * Kill a video player based on the process ID and reap the child process.
 *========================================================================*/
void
killPlayer( pid_t pid )
{
    int status;
    kill(pid, SIGTERM);
    pid = waitpid( pid, &status, 0);
}


/*
 ******************************************************************************
 ******************************************************************************
 *
 * Thread functions
 *
 ******************************************************************************
 ******************************************************************************
 */

/*========================================================================
 * Name:   isPlayerProcessorRunning
 * Prototype:  int isPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe read of playerIsRunning variable.
 *========================================================================*/
static int
isPlayerProcessorRunning( void )
{
    int status;
    pthread_mutex_lock( &playerProcessorMutex );
    status = playerIsRunning;
    pthread_mutex_unlock( &playerProcessorMutex );
    return status;
}

/*========================================================================
 * Name:   setPlayerProcessorRunning
 * Prototype:  int setPlayerProcessorRunning( void )
 *
 * Description:
 * Thread-safe set of playerIsRunning variable.
 *========================================================================*/
static void
setPlayerProcessorRunning( int val )
{
    pthread_mutex_lock( &playerProcessorMutex );
    playerIsRunning = val;
    pthread_mutex_unlock( &playerProcessorMutex );
}

/*========================================================================
 * Name:   playerProcessor
 * Prototype:  void playerProcessor( CLI_T * )
 *
 * Description:
 * Grab entries from player and process them.
 *
 * Input Arguments:
 * void *arg    Cast to CLI_T to get run time configuration data.
 *
 * Notes:
 * This loop runs every 100ms but only runs the queue every 3 seconds.
 * This allows the thread to exit quickly when the daemon is shutting down
 * while giving remote users plenty of time to send heartbeats for their streams.
 *========================================================================*/
static void *
playerProcessor( void *arg )
{
    int status = -1;
    char cmd[MAXBUF];

    // Spawn the video player
    videoPid = spawnPlayer((char *)arg);
    if ( videoPid == -1 )
        return(0);
    setPlayerProcessorRunning(1);

    // Wait to reap child 
    videoPid = waitpid( videoPid, &status, 0);
    videoPid = -1;

    // Now tell the X.org display to refresh in case we trashed the framebuffer.
    // Wait a bit for the hdmi to catch up.
    // Changing VTs was the only way to guarantee the display was updated with
    // X updates.
    system("xrefresh -display :0.0");
    sleep(1);

    sprintf(cmd, "chvt %s", cliOptions.vttmp);
    piboxLogger(LOG_INFO, "VT first switch command: %s\n",cmd);
    system(cmd);
    usleep(500);

    sprintf(cmd, "chvt %s", cliOptions.vt);
    piboxLogger(LOG_INFO, "VT second switch command: %s\n",cmd);
    system(cmd);
    usleep(500);

    system("xrefresh -display :0.0");
    
    setPlayerProcessorRunning(0);

    piboxLogger(LOG_INFO, "Player thread is exiting.\n");
    return(0);
}

/*========================================================================
 * Name:   startPlayerProcessor
 * Prototype:  void startPlayerProcessor( void )
 *
 * Description:
 * Setup thread to handle inbound messages.
 *
 * Notes:
 * Threads run until configs->serverEnabled = 0.  See shutdownPlayerProcessor().
 *========================================================================*/
void
startPlayerProcessor( char *filename )
{
    int rc;

    // Prevent running two at once.
    if ( isPlayerProcessorRunning() )
        return;

    /* Create a thread to expire streams. */
    rc = pthread_create(&playerProcessorThread, NULL, &playerProcessor, (void *)filename);
    if (rc)
    {
        piboxLogger(LOG_ERROR, "%s: Failed to create player thread: %s\n", PROG, strerror(rc));
        exit(-1);
    }
    piboxLogger(LOG_INFO, "%s: Started player thread.\n", PROG);
    return;
}

/*========================================================================
 * Name:   shutdownPlayerProcessor
 * Prototype:  void shutdownPlayerProcessor( void )
 *
 * Description:
 * Shut down message processing thread.
 *========================================================================*/
void
shutdownPlayerProcessor( void )
{
    int timeOut = 0;

    // Try to kill any running video.
    if ( videoPid != -1 )
        killPlayer(videoPid);

    if ( isPlayerProcessorRunning() )
    {
        while ( isPlayerProcessorRunning() )
        {
            sleep(1);
            timeOut++;
            if (timeOut == 60)
            {
                piboxLogger(LOG_ERROR, "Timed out waiting on player thread to shut down.\n");
                return;
            }
        }
        pthread_detach(playerProcessorThread);
    }
    piboxLogger(LOG_INFO, "playerProcessor shut down.\n");
}

