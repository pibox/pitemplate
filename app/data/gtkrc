# Good colors
# Light: a0cfa1
# Dark, but saturated: 2f8a30
# Very dark, low saturation: 192b19

style "theme-vbox"
{
	bg[NORMAL]   = "#000000" 
	base[NORMAL] = "#000000" 
}

style "theme-default"
{
  GtkButton      ::default_border    = { 0, 0, 0, 0 }
  GtkRange       ::trough_border     = 0
  GtkPaned       ::handle_size       = 6
  GtkRange       ::slider_width      = 8
  GtkRange       ::stepper_size      = 15

  GtkScrollbar   ::min_slider_length = 30
  GtkCheckButton ::indicator_size    = 14
  GtkMenuBar     ::internal-padding  = 0
  GtkTreeView    ::expander_size     = 14
  GtkExpander    ::expander_size     = 16
  GtkScale       ::slider-length     = 24
  GtkWidget::cursor_color           		= "#ffffff"
  GtkWidget::secondary_cursor_color 	= "#ffffff"
  
  xthickness = 1
  ythickness = 1

 	fg[NORMAL]		= "#BFBFBF" # Metacity and mouseover, Most text
	fg[PRELIGHT]      	= "#a0cfa1" # Text when mouseover
	fg[ACTIVE]	  	= "#bfbfbf" # Text when mouseclicking button, Tabs, Active window list
	fg[SELECTED]      	= "#bfbfbf" # Metacity X when window selected
	fg[INSENSITIVE]   	= "#303030" # Insensitive Text

	bg[NORMAL]	  	= "#222222" # Normal Background, inactive Metacity bar, buttons
	bg[PRELIGHT]	  	= "#282828" # Mouseover buttons
	bg[ACTIVE]	  	= "#2e2e2e" # Mouseclicking, Tabs, active window list
	bg[SELECTED]	        = "#2f8a30" # Metacity Bar
	bg[INSENSITIVE]   	= "#2e2e2e" # Insensitive buttons
	
	base[NORMAL]	        = "#222222" # Background, most
	base[PRELIGHT]	        = "#2e2e2e" # Mouseover menu
	base[ACTIVE]	  	= "#2e2e2e" # Menu active item in inactive window
	base[SELECTED]	        = "#2e2e2e" # Menu active item in active window
	base[INSENSITIVE] 	= "#323232" # Background, insensitive
	
	text[NORMAL]	  	= "#bfbfbf" # Text in window
	text[PRELIGHT]	        = "#a0cfa1" # Text on Mouseover
	text[ACTIVE]	  	= "#bfbfbf" # Active text in inactive window
	text[SELECTED]    	= "#a0cfa1" # Active text in active window
	text[INSENSITIVE] 	= "#bfbfbf" # Unknown

  
  engine "murrine" 
  {
	menuitemstyle = 0 # 0 = flat, 1 = glassy, 2 = striped
	scrollbar_color = "#acacac"
	contrast = 1.0
	glazestyle = 0 # 0 = flat hilight, 1 = curved hilight, 2 = concave style
	menubarstyle = 0 # 0 = flat, 1 = glassy, 2 = gradient, 3 = striped
	menubaritemstyle = 1 # 0 = menuitem look, 1 = button look
	menuitemstyle = 0 # 0 = flat, 1 = glassy, 2 = striped
	listviewheaderstyle = 0 # 0 = flat, 1 = glassy
	roundness = 2 # 0 = squared, 1 = old default, more will increase roundness
    	animation = TRUE # FALSE = disabled, TRUE = enabled
  }
}


style "theme-wide" = "theme-default"
{
  xthickness = 2
  ythickness = 2
}

style "theme-wider" = "theme-default"
{
  xthickness = 3
  ythickness = 3
}

style "theme-entry" = "theme-wider"
{
  bg[SELECTED]	  = "#323232"
}

style "theme-button" = "theme-wider"
{
  bg[NORMAL]      = "#303030"
  bg[ACTIVE]      = "#222222"
 }

style "theme-notebook" = "theme-wide"
{

}

style "theme-tasklist" = "theme-default"
{
  xthickness = 5
  ythickness = 3
}

style "theme-menu" = "theme-default"
{
  xthickness = 2
  ythickness = 1
 bg[NORMAL] = "#222222" 
}

style "theme-menu-item" = "theme-default"
{
  ythickness = 3
  fg[PRELIGHT] = "#a0cfa1"
  text[PRELIGHT] = "#a0cfa1"
  bg[SELECTED] = "#323232" 
}

style "theme-menubar" = "theme-default"
{
  bg[NORMAL] = "#222222"
}

style "theme-menubar-item"
{
	ythickness = 4
	bg[PRELIGHT] = "#323232"
}

style "theme-tree" = "theme-default"
{
  xthickness = 2
  ythickness = 2
}

style "theme-frame-title" = "theme-default"
{
  fg[NORMAL] = "#bfbfbf"
}

style "theme-tooltips" = "theme-default"
{
  xthickness = 4
  ythickness = 4
  bg[NORMAL] = "#2c2c2c"
}

style "theme-progressbar" = "theme-wide"
{
  xthickness = 1
  ythickness = 1
  fg[PRELIGHT]  = "#000000"
}

style "theme-combo" = "theme-button"
{
}

# widget styles
class "GtkWidget"      style "theme-default"
# class "GtkHBox"        style "theme-vbox"
# class "GtkVBox"        style "theme-vbox"
class "GtkLabel"       style "theme-vbox"
class "GtkButton"      style "theme-button"
class "GtkScale"       style "theme-button"
class "GtkCombo"       style "theme-button"
class "GtkRange"       style "theme-wide"
class "GtkFrame"       style "theme-wide"
class "GtkMenu"        style "theme-menu"
class "GtkEntry"       style "theme-entry"
class "GtkMenuItem"    style "theme-menu-item"
class "GtkNotebook"    style "theme-notebook"
class "GtkProgressBar" style "theme-progressbar"
class "*MenuBar*"      style "theme-menubar"

widget_class "*MenuItem.*" style "theme-menu-item"
widget_class "*MenuBar.*"  style "theme-menubar-item"

# combobox stuff
widget_class "*.GtkComboBox.GtkButton" style "theme-combo"
widget_class "*.GtkCombo.GtkButton"    style "theme-combo"
# tooltips stuff
widget_class "*.tooltips.*.GtkToggleButton" style "theme-tasklist"
widget "gtk-tooltips" style "theme-tooltips"

# treeview stuff
widget_class "*.GtkTreeView.GtkButton" style "theme-tree"
widget_class "*.GtkCTree.GtkButton" style "theme-tree"
widget_class "*.GtkList.GtkButton" style "theme-tree"
widget_class "*.GtkCList.GtkButton" style "theme-tree"
widget_class "*.GtkFrame.GtkLabel" style "theme-frame-title"

# notebook stuff
widget_class "*.GtkNotebook.*.GtkEventBox" style "theme-notebook"
widget_class "*.GtkNotebook.*.GtkViewport" style "theme-notebook"

# Font
style "myfont" {
    font_name = "Nunito 22"
}
widget_class "*" style "myfont"
gtk-font-name = "Nunito 22"
